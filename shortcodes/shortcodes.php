<?php
/*
	Innany Shortcode Module for Innany themes
	Version:  1.0.0
*/

if ( is_admin() ) {
	/*-----------------------------------------------------------------------------------*/
	# Register main Scripts and Styles
	/*-----------------------------------------------------------------------------------*/
	function innany_admin_register() {
		wp_enqueue_script( 'shortcodes-js', plugins_url( '/js/shortcodes.js', __FILE__ ), array('jquery'), 1.0, false); 

	    $categories = get_categories();
		$category_list = array();  
	    foreach ($categories as $category) {
	        if ( $category->slug == 'uncategorized') continue;
	        $category_list[] = array("text" => $category->cat_name, "value" => $category->slug);
	    }

		$innany_lang = array( 				
			"shortcode_innany"		=> esc_html__( 'Innany Shortcodes', 'innany' ), 
			"shortcode_category_list"	=> $category_list, 
			"shortcode_cat"				=> esc_html__( 'Category', 'innany' ), 
			"shortcode_number_of_posts"	=> esc_html__( 'Number of posts', 'innany' ), 
			"shortcode_1st_post_exc_length"	=> esc_html__( '1st post excerpt length', 'innany' ), 
			"shortcode_show_item"		=> esc_html__( 'Show item', 'innany' ), 
			"shortcode_cat_block_style_slider"	=> esc_html__( 'Category Block Slider', 'innany' ), 
			"shortcode_cat_slug"		=> esc_html__( 'Category Slug', 'innany' ), 
			"shortcode_cat_slug_description"		=> esc_html__( "To get category slug go to <a style='color:green; font-weight:bold;' target='_blank' href='edit-tags.php?taxonomy=category'>here.</a> To add multiple category <br> add with comma like this => cat_slug1, cat_slug2<br><span style='font-weight:bold;'>Note :</span> If you don't give any slug it will show all latest posts", 'innany' ), 
			"shortcode_grid"			=> esc_html__( 'Grid', 'innany' ), 
			"shortcode_masonry"			=> esc_html__( 'Masonry', 'innany' ), 
			"shortcode_right"			=> esc_html__( 'Right', 'innany' ), 
			"shortcode_left"			=> esc_html__( 'Left', 'innany' ), 
			"shortcode_top"				=> esc_html__( 'Top', 'innany' ), 
			"shortcode_bottom"			=> esc_html__( 'Bottom', 'innany' ), 
			"shortcode_center"			=> esc_html__( 'Center', 'innany' ), 
			"shortcode_width"			=> esc_html__( 'Width', 'innany' ), 
			"shortcode_content"			=> esc_html__( 'Content', 'innany' ), 
			"shortcode_button"			=> esc_html__( 'Button', 'innany' ), 
			"shortcode_type"			=> esc_html__( 'Type', 'innany' ), 
			"shortcode_default"			=> esc_html__( 'Default', 'innany' ), 
			"shortcode_primary"			=> esc_html__( 'Primary', 'innany' ), 
			"shortcode_success"			=> esc_html__( 'Success', 'innany' ), 
			"shortcode_info"			=> esc_html__( 'Info', 'innany' ), 
			"shortcode_warning"			=> esc_html__( 'Warning', 'innany' ), 
			"shortcode_danger"			=> esc_html__( 'Danger', 'innany' ),
			"shortcode_size"			=> esc_html__( 'Size', 'innany' ),
			"shortcode_size_large"		=> esc_html__( 'Large', 'innany' ), 
			"shortcode_size_default" 	=> esc_html__( 'Default', 'innany' ), 
			"shortcode_size_small"		=> esc_html__( 'Small', 'innany' ), 
			"shortcode_size_ex_small"	=> esc_html__( 'Extra Small', 'innany' ), 
			"shortcode_progressbar"		=> esc_html__( 'Progress Bar', 'innany' ), 
			"shortcode_link"			=> esc_html__( 'Link', 'innany' ), 
			"shortcode_text"			=> esc_html__( 'Text', 'innany' ), 
			"shortcode_title"			=> esc_html__( 'Title', 'innany' ), 
			"shortcode_state"			=> esc_html__( 'State', 'innany' ), 
			"shortcode_status"			=> esc_html__( 'Status', 'innany' ), 
			"shortcode_url"				=> esc_html__( 'URL', 'innany' ), 
			"shortcode_background_img"	=> esc_html__( 'Background Image', 'innany' ), 
			"shortcode_opened"			=> esc_html__( 'Opened', 'innany' ), 
			"shortcode_closed"			=> esc_html__( 'Closed', 'innany' ), 
			"shortcode_true"			=> esc_html__( 'True', 'innany' ), 
			"shortcode_false"			=> esc_html__( 'False', 'innany' ), 
			"shortcode_alert"			=> esc_html__( 'Alert', 'innany' ), 
			"shortcode_dismiss"			=> esc_html__( 'Dismiss', 'innany' ), 
			"shortcode_striped"			=> esc_html__( 'Striped', 'innany' ), 
			"shortcode_animation"		=> esc_html__( 'Animation', 'innany' ), 
			"shortcode_height"			=> esc_html__( 'Height:', 'innany' ), 
			"shortcode_video"			=> esc_html__( 'Video', 'innany' ), 
			"shortcode_video_url"		=> esc_html__( 'Video URL:', 'innany' ), 
			"shortcode_audio"			=> esc_html__( 'Audio', 'innany' ), 
			"shortcode_mp3"				=> esc_html__( 'MP3 file URL', 'innany' ), 
			"shortcode_m4a"				=> esc_html__( 'M4A file URL', 'innany' ), 
			"shortcode_ogg"				=> esc_html__( 'OGG file URL', 'innany' ), 
			"shortcode_tooltip"			=> esc_html__( 'ToolTip', 'innany' ), 
			"shortcode_direction"		=> esc_html__( 'Direction', 'innany' ), 
			"shortcode_facebook"		=> esc_html__( 'Facebook', 'innany' ), 
			"shortcode_twitter"			=> esc_html__( 'Twitter', 'innany' ), 
			"shortcode_google_plus"		=> esc_html__( 'Google Plus', 'innany' ),
			"shortcode_dropcap"			=> esc_html__( 'Dropcap', 'innany' ),
			"shortcode_columns"			=> esc_html__( 'Columns', 'innany' ),
			"shortcode_accordion"		=> esc_html__( 'Accordion', 'innany' ),
			"shortcode_tab"				=> esc_html__( 'Tab', 'innany' ),
			"shortcode_custom_icon"		=> esc_html__( 'Insert Custom Icon', 'innany' ),
			);		
		wp_localize_script( 'shortcodes-js', 'innanyLang', $innany_lang );	
	}
	add_action( 'admin_enqueue_scripts', 'innany_admin_register' ); 
} // end is_admin()
 
add_action('admin_head', 'innany_add_mce_button');
function innany_add_mce_button() {
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'innany_add_tinymce_plugin' );
		add_filter( 'mce_buttons', 'innany_register_mce_button' );
	}
}

// Declare script for new button
function innany_add_tinymce_plugin( $plugin_array ) {
	$plugin_array['innany_mce_button'] = plugins_url( '/mce.js', __FILE__ );
	return $plugin_array;
}

// Register new button in the editor
function innany_register_mce_button( $buttons ) {
	array_push( $buttons, 'innany_mce_button' );
	return $buttons;
}


// status shortcode for facebook and twitter
add_shortcode("status", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'url' => '',
			'background' => '',
		), $atts
	);
	extract($atts);

	( !empty($background) ) ? $background = $background : $background = "";
	$status_facebook = ( $type == "facebook" ) ? "facebook" : "";
	$status_twitter = ( $type == "twitter" ) ? "twitter" : "";
	$status_gplus = ( $type == "google_plus" ) ? "google_plus" : "";

	if( !empty( $status_facebook ) || !empty( $status_twitter ) || !empty( $status_gplus ) ):	?>
		<div class="post-status-wrapper" style="background: url(<?php echo $background; ?>);">
		<?php if( !empty( $status_facebook ) ) : ?>
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;  js = d.createElement(s);
				js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-post" data-href="<?php echo $url ?>"></div>
		<?php elseif( !empty( $status_twitter ) ) : ?>
			<blockquote class="twitter-tweet"><a href="<?php echo $url ?>"></a></blockquote>
			<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		<?php elseif( !empty( $status_gplus ) ) : ?>
			<script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
			<div class="g-post" data-href="<?php echo $url ?>"></div>
		<?php endif; ?>
		</div><!-- /.post-status-wrapper -->
	<?php endif; 
	$status = ob_get_clean();
	return $status;
});

// bootstrap column grid row shortcode
add_shortcode("row", function($atts, $content = null) {
	return '<div class="row">' . do_shortcode( $content ) . '</div>';
});

// bootstrap column grid shortcode
add_shortcode("column", function($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'number' => '',
			'offset' => '',
		), $atts
	);
	extract($atts);
	if ( !empty($offset) ) {
		$offset = "col-md-offset-$offset";
	}
	return "<div class='$offset col-md-$number'>". do_shortcode( $content ) ."</div>";
});

// bootstrap progress bar shortcode 
add_shortcode("progress_bar", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'width' => '50',
			'title' => '',
			'striped' => false,
			'animation' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " progress-bar-$type " : $type = "";
	( $striped == true ) ? $striped = " progress-bar-striped " : $striped = "";
	( $animation == true ) ? $animation = " active " : $animation = "";
	?>

	<div class="progress">
		<div class="progress-bar <?php echo $type.$striped.$animation;?>" role="progressbar" aria-valuenow="<?php echo $width; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $width; ?>%">
	    <?php echo $title; ?>
	  	</div> <!-- /.progress-bar -->
	</div> <!-- /.progress -->

	<?php	
	$progress_bar = ob_get_clean();
	return $progress_bar;
});

// bootstrap button shortcode 
add_shortcode("button", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => 'primary',
			'text' => '',
			'size' => '',
			'disabled' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " btn-$type " : $type = "";
	( $disabled == true ) ? $disabled = " disabled='true' " : $disabled = "";
	( $text == null ) ? $text = __(" Button ", "innany") : $text = $text;
	?>

	<button type="button" class="btn <?php echo $type." ".$size; ?>" <?php echo $disabled; ?>>
	<?php echo $text; ?>
	</button>

	<?php	
	$button = ob_get_clean();
	return $button;
});


// bootstrap accordion shortcode collapse_group
add_shortcode("collapse_group", function($atts, $content = null) {
	return '<div class="panel-group" id="accordion" role="tablist" aria-multiselecategoryTable="true">' . do_shortcode( $content ) . '</div>';
});

// bootstrap accordion shortcode collapse 
add_shortcode("collapse", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'id' => '',
			'title' => '',
			'expanded' => "false",
		), $atts
	);
	extract($atts);
	( $expanded == "true" ) ? $aria_expander = "true" : $aria_expander = "false";
	( $expanded == "true" ) ? $expanded_two = "" : $expanded_two = "collapsed";
	( $expanded == "true" ) ? $expanded_in = "in" : $expanded_in = "";
	?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
		  <h4 class="panel-title">
		    <a class="accordion-toggle <?php echo $expanded_two; ?>" role="button" data-toggle="collapse" href="#<?php  echo $id; ?>" aria-expanded="<?php echo $aria_expander; ?>" >
		    <?php  echo $title; ?>
		    </a>
		  </h4>
		</div>
		<div id="<?php echo $id; ?>" class="panel-collapse collapse <?php echo $expanded_in; ?>" role="tabpanel"  aria-expanded="<?php echo $aria_expander; ?>">
		  <div class="panel-body">
		  <?php  echo $content; ?>
		  </div>
		</div>
	</div>
	<?php	
	$collapse = ob_get_clean();
	return $collapse;
});

// bootstrap shortcode alert 
add_shortcode("alert", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'type' => '',
			'dismiss' => false,
		), $atts
	);
	extract($atts);
	?>
	<div class="alert alert-<?php echo $type;?> " role="alert">
	<?php
	  if ( $dismiss == true ) {
	  	?>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?php
	  }
	  echo $content;
	?>
	</div><!-- /.alert -->
	<?php	
	$alert = ob_get_clean();
	return $alert;
});

// bootstrap shortcode tooltip 
add_shortcode("tooltip", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'text' => '',
			'direction' => "top",
		), $atts
	);
	extract($atts);
	?>
	<span data-toggle="tooltip" title="<?php echo $text; ?>" data-placement="<?php echo $direction; ?>"><?php echo $content; ?></span>

	<?php	
	$tooltip = ob_get_clean();
	return $tooltip;
});

// bootstrap shortcode dropcap 
add_shortcode("dropcap", function($atts, $content = null) {
	ob_start();	
	?>
	<span class="dropcap"><?php echo $content; ?></span>
	<?php	
	$dropcap = ob_get_clean();
	return $dropcap;
});

// bootstrap tabs shortcode 
$tabs_divs = '';

function tabs_group($atts, $content = null ) {
    global $tabs_divs;
    $tabs_divs = '';
    $output = '<div id="tab-side-container"><ul class="nav nav-tabs"';
    $output.='>'.do_shortcode($content).'</ul>';
    $output.= '<div class="tab-content">'.$tabs_divs.'</div></div>';
    return $output;  
}  


function tab($atts, $content = null) {  
    global $tabs_divs;
    extract(shortcode_atts(array(  
        'id' => '',
        'title' => '', 
        'active' => '', 
    ), $atts));  
    if(empty($id))
        $id = 'side-tab'.rand(100,999);
    $output = '
        <li class="'.$active.'">
            <a href="#'.$id.'" data-toggle="tab">'.$title.'</a>
        </li>
    ';
    $tabs_divs.= '<div class="tab-pane '.$active.'" id="'.$id.'">'.$content.'</div>';
    return $output;
}

add_shortcode('tabs', 'tabs_group');
add_shortcode('tab', 'tab');

// custom icon font
add_shortcode("custom_icon", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'name' => '',
			'class' => '',
			'font_size' => '',
			'color' => '',
		), $atts
	);
	extract($atts); ?>
	<i class="<?php echo esc_attr($name); ?> <?php echo esc_attr($class); ?>" style="font-size:<?php echo esc_attr($font_size); ?>; color: <?php echo esc_attr($color); ?>;"></i>
	<?php	
	$custom_icon = ob_get_clean();
	return $custom_icon;
});