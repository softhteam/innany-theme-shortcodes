(function() {
	tinymce.PluginManager.add('innany_mce_button', function( editor, url ) {
		editor.addButton( 'innany_mce_button', {
            text: innanyLang.shortcode_innany, 
            icon: false,
			tooltip: innanyLang.shortcode_innany,
			type: 'menubutton',
			minWidth: 210,
			menu: [
				{
					text: innanyLang.shortcode_button,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_button,
							body: [
								{
									type: 'listbox',
									name: 'ButtonType',
									label: innanyLang.shortcode_type,
									'values': [
										{text: innanyLang.shortcode_default, value: 'btn-default'},
										{text: innanyLang.shortcode_primary, value: 'btn-primary'},
										{text: innanyLang.shortcode_success, value: 'btn-success'},
										{text: innanyLang.shortcode_info, value: 'btn-info'},
										{text: innanyLang.shortcode_warning, value: 'btn-warning'},
										{text: innanyLang.shortcode_danger, value: 'btn-danger'},
									]
								},
								{
									type: 'listbox',
									name: 'ButtonSize',
									label: innanyLang.shortcode_size,
									'values': [
										{text: innanyLang.shortcode_size_large, value: 'btn-lg'},
										{text: innanyLang.shortcode_size_default, value: ''},
										{text: innanyLang.shortcode_size_small, value: 'btn-sm'},
										{text: innanyLang.shortcode_size_ex_small, value: 'btn-xs'},
									]
								},
								{
									type: 'textbox',
									name: 'ButtonText',
									label: innanyLang.shortcode_text,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[button type="' + e.data.ButtonType + '" size="' + e.data.ButtonSize + '" text="' + e.data.ButtonText + '"]');
							}
						});
					} // onclick
				},
				{
					text: innanyLang.shortcode_progressbar,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_progressbar,
							body: [
								{
									type: 'listbox',
									name: 'ProgressbarType',
									label: innanyLang.shortcode_type,
									'values': [
										{text: innanyLang.shortcode_default, value: 'default'},
										{text: innanyLang.shortcode_primary, value: 'primary'},
										{text: innanyLang.shortcode_success, value: 'success'},
										{text: innanyLang.shortcode_info, value: 'info'},
										{text: innanyLang.shortcode_warning, value: 'warning'},
										{text: innanyLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'textbox',
									name: 'ProgressbarWidth',
									label: innanyLang.shortcode_width,
									minWidth: 300,
									value: '50'
								},
								{
									type: 'textbox',
									name: 'ProgressbarTitle',
									label: innanyLang.shortcode_title,
									minWidth: 300,
									value: ''
								},
								{
									type: 'listbox',
									name: 'ProgressbarStriped',
									label: innanyLang.shortcode_striped,
									'values': [
										{text: innanyLang.shortcode_true, value: 'true'},
										{text: innanyLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'listbox',
									name: 'ProgressbarAnimation',
									label: innanyLang.shortcode_animation,
									'values': [
										{text: innanyLang.shortcode_true, value: 'true'},
										{text: innanyLang.shortcode_false, value: 'false'},
									]
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[progress_bar type="' + e.data.ProgressbarType + '" width="' + e.data.ProgressbarWidth + '" title="' + e.data.ProgressbarTitle + '" striped="' + e.data.ProgressbarStriped + '" animation="' + e.data.ProgressbarAnimation + '"]');
							}
						});
					}
				},
				{
					text: innanyLang.shortcode_status,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_status,
							body: [
								{
									type: 'listbox',
									name: 'StatusType',
									label: innanyLang.shortcode_type,
									'values': [
										{text: innanyLang.shortcode_facebook, value: 'facebook'},
										{text: innanyLang.shortcode_twitter, value: 'twitter'},
										{text: innanyLang.shortcode_google_plus, value: 'google_plus'},
									]
								},
								{
									type: 'textbox',
									name: 'StatusURL',
									label: innanyLang.shortcode_url,
									minWidth: 300,
									value: ''
								},
								{
									type: 'textbox',
									name: 'StatusBackground',
									label: innanyLang.shortcode_background_img,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[status type="' + e.data.StatusType + '" url="' + e.data.StatusURL + '" background="' + e.data.StatusBackground + '"]');
							}
						});
					}
				},
				{
					text: innanyLang.shortcode_alert,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_alert,
							body: [
								{
									type: 'listbox',
									name: 'AlertType',
									label: innanyLang.shortcode_type,
									'values': [
										{text: innanyLang.shortcode_default, value: 'default'},
										{text: innanyLang.shortcode_primary, value: 'primary'},
										{text: innanyLang.shortcode_success, value: 'success'},
										{text: innanyLang.shortcode_info, value: 'info'},
										{text: innanyLang.shortcode_warning, value: 'warning'},
										{text: innanyLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'listbox',
									name: 'AlertDismiss',
									label: innanyLang.shortcode_dismiss,
									'values': [
										{text: innanyLang.shortcode_true, value: 'true'},
										{text: innanyLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'textbox',
									name: 'AlertContent',
									label: innanyLang.shortcode_content,
									value: '',									
									multiline: true,
									minWidth: 300,
									minHeight: 100
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[alert type="' + e.data.AlertType + '" dismiss="' + e.data.AlertDismiss + '"]' + e.data.AlertContent + '[/alert]');
							}
						});
					}
				},
				{
					text: innanyLang.shortcode_video,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_video,
							body: [
								{
									type: 'textbox',
									name: 'VideoURL',
									label: innanyLang.shortcode_video_url,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'VideoWidth',
									label: innanyLang.shortcode_width,
									value: ''
								},
								{
									type: 'textbox',
									name: 'Videoheight',
									label: innanyLang.shortcode_height,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[embed width="' + e.data.VideoWidth + '" height="' + e.data.Videoheight + '"]' + e.data.VideoURL + '[/embed]');
							}
						});
					}
				},
				{
					text: innanyLang.shortcode_audio,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_audio,
							body: [
								{
									type: 'textbox',
									name: 'mp3URL',
									label: innanyLang.shortcode_mp3,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'm4aURL',
									label: innanyLang.shortcode_m4a,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'oggURL',
									label: innanyLang.shortcode_ogg,
									value: 'http://',
									minWidth: 300,
								},
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[audio mp3="' + e.data.mp3URL + '" m4a="' + e.data.m4aURL + '" ogg="' + e.data.oggURL + '"]');
							}
						});
					}
				},
				{
					text: innanyLang.shortcode_tooltip,
					onclick: function() {
						editor.windowManager.open( {
							title: innanyLang.shortcode_tooltip,
							body: [
								{
									type: 'textbox',
									name: 'TooltipText',
									label: innanyLang.shortcode_text,
									value: '',
									minWidth: 300,
								},
								{
									type: 'listbox',
									name: 'TooltipDirection',
									label: innanyLang.shortcode_direction,
									'values': [
										{text: innanyLang.shortcode_top, value: 'top'},
										{text: innanyLang.shortcode_right, value: 'right'},
										{text: innanyLang.shortcode_bottom, value: 'bottom'},
										{text: innanyLang.shortcode_left, value: 'left'},
									]
								},
								{
									type: 'textbox',
									name: 'ToolTipContent',
									label: innanyLang.shortcode_content,
									value: '',
									multiline: true,
									minWidth: 300,
									minHeight: 100
								}
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[tooltip text="' + e.data.TooltipText + '" direction="' + e.data.TooltipDirection + '"]'+ e.data.ToolTipContent +'[/tooltip]');
							}
						});
					}
				},
				{
					text: innanyLang.shortcode_columns,
						onclick: function() {
							editor.insertContent( '\
							[row]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
							[/row]');
						}
				},
				{
					text: innanyLang.shortcode_accordion,
						onclick: function() {
							editor.insertContent( '\
							[collapse_group]<br />\
								[collapse id="accordion_one" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
								[collapse id="accordion_two" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
							[/collapse_group]');
						}
				},
				{
					text: innanyLang.shortcode_tab,
						onclick: function() {
							editor.insertContent( '\
							[tabs]<br />\
								[tab id="tab_one" title="Tab Title" active="active"] Tab Content [/tab]<br />\
								[tab id="tab_two" title="Tab Title"] Tab Content [/tab]<br />\
								[tab id="tab_three" title="Tab Title"] Tab Content [/tab]<br />\
							[/tabs]');
						}
				},
				{
                    text: innanyLang.shortcode_custom_icon,
                    classes: 'sh-custom-icon',
                },
			]
		});
	});
})();